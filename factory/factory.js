function Apple(specs) {
    this.weight = specs.weight;
    this.color = specs.color;
    this.description =  'Apple weights ' + this.weight + ' kg. It is ' + this.color;
}

function Melon(specs) {
    this.weight = specs.weight;
    this.color = specs.color;
    this.description = 'Melon weights ' + this.weight + ' kg. It is ' + this.color;
}


function Fruits() {
    return {
    create : function(specs) {
        switch (specs.name) {
            case 'apple' : return new Apple(specs);
            case 'melon' : return new Melon(specs);
        }
    }
   }
}

var fruits = new Fruits();

var apple = fruits.create({
    name  : 'apple',
    weight : 10,
    color : 'yellow'
});

var melon = fruits.create({
    name  : 'melon',
    weight : 20,
    color : 'green'
});

console.log(apple.description);
console.log(melon.description);

