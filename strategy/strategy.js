var Credentials1 = {
    name : 'alex',
    password : '12345',
    firstName : 'Alex'
};
var Credentials2 = {
    name : 'john',
    password : '12345',
    firstName : 'John',
    lastName : 'Doe'
};

var Db = [
    {
        name : 'alex',
        password : '123456',
        firstName : 'Alex'
    },
    {
        name : 'john',
        password : '12345',
        firstName : 'John',
        lastName : 'Doe'
    }

];

function Strategy(InputCredentials, db){
    db.forEach(function(item, i, arr){
        if (InputCredentials.name === item.name) {
            var greetings = 'Welcome';
            if (InputCredentials.password !== item.password) {
                console.log('Credentials incorrect');
            }
            else {
                console.log('Access granted');

                if (InputCredentials.firstName.length > 0) {
                    greetings += ', ' + InputCredentials.firstName;

                    if (InputCredentials.lastName.length > 0) {
                        greetings += ' ' + InputCredentials.lastName;
                    }
                }
                console.log(greetings + '!')
            }

        }
    });
}

var strategy1 = new Strategy(Credentials1, Db);
var strategy2 = new Strategy(Credentials2, Db);