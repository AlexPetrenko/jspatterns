function Observer() {
    return {
        handlers : [],

        subscribe : function(fn) {
            this.handlers.push(fn);
        },

        unsubscribe : function (fn) {
            this.handlers = this.handlers.filter(
                function (item) {
                    if (item !== fn) {
                        return item;
                    }
                }
            );
        },

        fire : function (o, thisObj) {
            var scope = thisObj || window;
            this.handlers.forEach(function (item) {
                item.call(scope, o);
            });
        }
    }
}

  var  bla = function(message){
        console.log(message);
    };

var observer = new Observer();


observer.subscribe(bla);
observer.fire('Mouse clicked 1');
observer.unsubscribe(bla);
observer.fire('Mouse clicked 2');
observer.subscribe(bla);
observer.fire('Mouse clicked 3');